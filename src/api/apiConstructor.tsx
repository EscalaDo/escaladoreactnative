import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

const apiConstructor = axios.create();


apiConstructor.interceptors.request.use(
   async (config) => {
       const token = await AsyncStorage.getItem('token');
       if( token ){
           config.headers!['Authorization'] = 'Bearer '+token;
       }
       return config;
   }
);

export default apiConstructor;
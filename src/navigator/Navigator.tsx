import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
// import { LoginOrSignUp } from '../screens/LoginOrSignUp';
// import { RoutesListView } from '../screens/RoutesListView';
import { AllRoutes } from '../screens/AllRoutesView';
import { OneRoute } from '../screens/OneRouteView';
import { AddNewRouteView } from '../screens/AddNewRouteView';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { UserFeedScreen } from '../screens/UserFeedScreens/UserFeedScreen';
import { AddTrainingWithExercices } from '../screens/AddTrainingWithExercices';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { useGetTokenUser } from '../hooks/GETS/User/useGetTokenUser';
import { LoginScreen } from '../screens/AuthScreens/LoginScreen';
import { RegisterScreen } from '../screens/AuthScreens/RegisterScreen';
import { AuthContext } from '../context/AuthContext';
import ShowCommentsFromRoute from '../screens/ShowCommentsFromRoute';
import { LoadingScreen } from '../screens/AuthScreens/LoadingScreen';
import { UserInfoScreen } from '../screens/UserFeedScreens/UserInfoScreen';
import { MyRoutesScreen } from '../screens/AuthScreens/MyRoutes/MyRoutesScreen';
import { TrainingFeedScreen } from '../screens/TrainingScreens/TrainingFeedScreen';
import { MyTrainingScreen } from '../screens/AuthScreens/MyTrainings/MyTrainingScreen';
import { OneTrainingScreen } from '../screens/TrainingScreens/OneTrainingScreen';
import { CommentsScreen } from '../screens/UserFeedScreens/CommentsScreen';
import { EditTraining } from '../screens/AuthScreens/MyTrainings/EditTraining';
// const Tab = createStackNavigator();
const Tab = createBottomTabNavigator();

export const Navigator = () => {

  const { status } = useContext(AuthContext)


  if (status === 'checking') return <LoadingScreen />


  return (
    <Tab.Navigator
      screenOptions={{
        tabBarInactiveBackgroundColor: 'white',
        // tabBarActiveBackgroundColor: 'pink',
        headerShown: false,
        tabBarActiveTintColor: '#F3872F', //para cambiar el color del icon
      }}

    >

      {
        (status !== 'authenticated')
          ? (
            <>
              <Tab.Screen options={() => ({
                tabBarButton: () => null,
                tabBarStyle: { display: 'none' }
              })} name="LoginScreen" component={LoginScreen} />
              <Tab.Screen options={() => ({ tabBarButton: () => null, tabBarStyle: { display: 'none' } })} name="RegisterScreen" component={RegisterScreen} />
            </>
          )
          : (
            <>
              <Tab.Screen options={() => ({
                tabBarLabel: 'Routes',
                tabBarIcon: ({ color, size }) => (
                  <Foundation name="mountains" color={color} size={size} />
                ),
              })} name="UserFeedScreen" component={UserFeedScreen} />

              <Tab.Screen options={() => ({
                tabBarLabel: 'Trainings',
                tabBarIcon: ({ color, size }) => (
                  <MaterialIcons name="fitness-center" color={color} size={size} />
                ),
              })} name="TrainingFeedScreen" component={TrainingFeedScreen} />

              <Tab.Screen options={() => ({
                tabBarLabel: 'User',
                tabBarIcon: ({ color, size }) => (
                  <FontAwesome name="user" color={color} size={size} />
                ),
              })} name="UserInfoScreen" component={UserInfoScreen} />

              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="OneRouteView" component={OneRoute} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="EditTraining" component={EditTraining} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="CommentsScreen" component={CommentsScreen} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="OneTrainingScreen" component={OneTrainingScreen} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="MyRoutesScreen" component={MyRoutesScreen} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="MyTrainingScreen" component={MyTrainingScreen} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="AddNewRouteView" component={AddNewRouteView} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="AddTrainingWithExercises" component={AddTrainingWithExercices} />
              <Tab.Screen options={() => ({ tabBarButton: () => null })} name="ShowCommentsFromRoute" component={ShowCommentsFromRoute} />
            </>
          )
      }

    </Tab.Navigator>
  );
}
import React, { createContext, useEffect, useReducer } from "react";
// import { apiConstructor } from "../api/apiConstructor";
import { LoginData, LoginResponse, User } from '../interfaces/models/modelAddRoutes';
import { authReducer, AuthState } from './AuthReducer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import apiConstructor from "../api/apiConstructor";
import { RegisterUser } from '../interfaces/models/models';

type AuthContextProps = {
    errorMessage: string;
    token: string | null;
    user: User | null;
    status: 'checking' | 'authenticated' | 'non-authenticated'
    signUp: (registerUser: RegisterUser) => void;
    signIn: (loginData: LoginData) => void;
    logOut: () => void;
    removeError: () => void;
}

const authInicialState: AuthState = {
    status: 'checking',
    token: null,
    user: null,
    errorMessage: ''
}

export const AuthContext = createContext({} as AuthContextProps);

export const AuthProvider = ({ children }: any) => {

    const [state, dispatch] = useReducer(authReducer, authInicialState)

    useEffect(() => {
        checkToken()
    }, [])


    const checkToken = async () => {
        console.log('Comprobando Token')
        const token = await AsyncStorage.getItem('token');
        console.log(token)
        //No token, no authetincado
        if (!token) return dispatch({ type: 'noAuthenticated' });

        // //Hay token
        const resp = await apiConstructor.get('http://152.228.170.234:9001/auth')

        if (resp.status !== 200) {
            return dispatch({ type: 'noAuthenticated' })
        }

        await AsyncStorage.setItem('token', resp.data.token)


        dispatch({
            type: 'signUp',
            payLoad: {
                token: resp.data.token,
                user: resp.data.user,
                message: resp.data.message,
            }
        });
    }


    const signUp = async (obj: RegisterUser) => {
        // try {

        //     const { data } = await apiConstructor
        //         .post('http://152.228.170.234:9001/auth/register',{ name, lastname, email, username, password, role}
        //         )

        //         console.log('ERROR PETITION',data)

        //     dispatch({type: 'noAuthenticated'});

        // } catch (error) {
        //     console.log('ERROR SIGNUP:',error)
        //     dispatch({
        //         type: 'addError',
        //         payLoad: `${error}`
        //     })

        // }


        try {
            const objSerialized = JSON.stringify(obj);

            const resp = await apiConstructor.post<LoginResponse>('http://152.228.170.234:9001/auth/register', objSerialized, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            if(resp.status !== 200){
                dispatch({
                    type: 'addError',
                    payLoad: `${resp.statusText}`
                })
            }

            dispatch({
                type: 'signUp',
                payLoad: {
                    token: resp.data.token,
                    user: resp.data.user,
                    message: resp.data.message,
                }
            });

            await AsyncStorage.setItem('token', resp.data.token ? resp.data.token : '')


        } catch (error) {
            dispatch({
                type: 'addError',
                payLoad: `${error}`
            })
        }

    };
    const signIn = async ({ username, password }: LoginData) => {
        try {

            const { data } = await apiConstructor.post<LoginResponse>('http://152.228.170.234:9001/auth/login', { username, password })

            dispatch({
                type: 'signUp',
                payLoad: {
                    token: data.token,
                    user: data.user,
                    message: data.message,
                }
            });

            await AsyncStorage.setItem('token', data.token ? data.token : '')

        } catch (error) {
            dispatch({
                type: 'addError',
                payLoad: `${error}`
            })

        }

    };
    const logOut = async () => {

        await AsyncStorage.removeItem('token')

        dispatch({
            type: 'logout'
        })
    };
    const removeError = () => {
        dispatch({ type: 'removeError' })
    };

    return (
        <AuthContext.Provider value={{
            ...state,
            signUp,
            signIn,
            logOut,
            removeError,
        }}>
            {children}
        </AuthContext.Provider>
    )

}
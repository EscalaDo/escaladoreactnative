import { User } from '../interfaces/models/modelAddRoutes';

export interface AuthState {
    errorMessage: string;
    token: string | null;
    user: User | null;
    status: 'checking' | 'authenticated' | 'non-authenticated'
}

type AuthAction =
    | { type: 'signUp', payLoad: { token: string | null, user: User | null, message: string | null } }
    | { type: 'addError', payLoad: string }
    | { type: 'removeError' }
    | { type: 'noAuthenticated' }
    | { type: 'logout' }

export const authReducer = (state: AuthState, action: AuthAction): AuthState => {
    switch (action.type) {
        case 'addError':
            return {
                ...state,
                user: null,
                status: 'non-authenticated',
                token: null,
                errorMessage: action.payLoad
            }
        case 'removeError':
            return {
                ...state,
                errorMessage: ''
            }
        case 'signUp':
            return {
                ...state,
                errorMessage: '',
                status: 'authenticated',
                token: action.payLoad.token,
                user: action.payLoad.user,
            }
        case 'logout':
        case 'noAuthenticated':
            return {
                ...state,
                status: 'non-authenticated',
                token: null,
                user: null,
            }
        default:
            return state;
    }
}

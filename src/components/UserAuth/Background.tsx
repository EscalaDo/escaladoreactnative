import React from 'react'
import { Dimensions, View } from 'react-native'

const {width, height} = Dimensions.get('screen');

export const Background = () => {
  return (
      <>
        <View 
            style={{
                position: 'absolute',
                backgroundColor: '#5856D6',
                width: width,
                height: height,
            }}
        />
      </>
  )
}

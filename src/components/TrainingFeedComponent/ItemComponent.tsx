import React, { useContext, useState } from 'react'
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View, ImageBackground } from 'react-native';
import { AuthContext } from '../../context/AuthContext'
import { Training, User } from '../../interfaces/models/modelAddRoutes';
import Icon from "react-native-vector-icons/Fontisto"
import apiConstructor from '../../api/apiConstructor';
import EditIcon from "react-native-vector-icons/MaterialIcons"


interface Props {
    training: Training,
    navigation: any,
}

const { width, height } = Dimensions.get('screen')

export const ItemComponent = ({ training, navigation }: Props) => {

    const { id, name, user, exercises, userTraining } = training;

    const { user: currentUser } = useContext(AuthContext);
    // const [heartIcon, setHeartIcon] = useState<string>(isFound ? 'heart' : 'heart-alt');


    const isFound = userTraining.some(({ id }) => {
        if (id === currentUser?.id) {
            return true;
        }

        return false;
    });

    const [heartIcon, setHeartIcon] = useState<string>(isFound ? 'heart' : 'heart-alt');


    const onPress = async () => {
        await apiConstructor
            .post<boolean>(`http://152.228.170.234:9001/saveTrainingForUser/${id}/${currentUser?.id}`)
            .then(resp => {
                resp.data ?
                    setHeartIcon('heart') :
                    setHeartIcon('heart-alt')
            });
    }

    return (

        <TouchableOpacity
            key={id}
            onPress={() => {
                navigation.navigate('OneTrainingScreen', training)
            }}
        >
            <View
                key={id}
                style={styles.container}
            >
                {/* <ImageBackground source={require('../../assets/img/logoExercise.png')}  style={{ flex: 3, justifyContent: 'center', height: height / 3}}> */}
                <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 20, opacity: 1, color: 'black', textTransform: 'uppercase', fontWeight: 'bold' }}>{name}</Text>
                </View>
                <View style={{ flex: 6, justifyContent: 'center', alignItems: 'center', opacity: 1 }}>
                    <Image source={require('../../assets/img/logoExercise.png')} style={{ height: 200, width: 200 }} />
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', top: 15 }}>
                    <Text style={{ textAlign: 'center', fontSize: 20, opacity: 1, color: 'black', textTransform: 'uppercase', fontWeight: 'normal' }}>Number of exercices: {exercises.length}</Text>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', padding: 10, marginTop: 10 }}>
                    <Icon
                        name={heartIcon}
                        size={20} color='#F3872F'
                        onPress={onPress} />
                </View>
                {/* </ImageBackground> */}
            </View>

        </TouchableOpacity >

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: width,
        height: height / 2.8,
        backgroundColor: 'white',
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },
    headerTraining: {
        backgroundColor: 'red'
    },
    headerRoute: {
        backgroundColor: 'red',
        height: 50,
    },
    body: {
        flex: 1,
        backgroundColor: 'yellow',
    },
    textTitle: {
        fontWeight: 'bold',
        color: 'white',
        textDecorationStyle: 'solid',
        fontSize: 20
    },
    textSubTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        color: 'white',
        textTransform: 'capitalize',
    },
    textFooter: {
        fontWeight: 'bold',
        fontSize: 15,
        color: 'white',
        bottom: 0
    },
    buttonFloating2: {
        // borderWidth: 1,
        // borderColor: 'blue',
        // borderRadius: 100,
        // backgroundColor: 'blue',
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        top: 0,
        right: 5,
        height: 70,
    }

});
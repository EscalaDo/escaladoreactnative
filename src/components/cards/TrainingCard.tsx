import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Training } from '../../interfaces/models/modelAddRoutes';
import Icon from 'react-native-vector-icons/Fontisto';


interface Props {
    training: Training,
    navigation: any,
}

export const TrainingCard = ({ training, navigation }: Props) => {
    const {id, name, user, exercises, } = training;

    return (

        <View
            key={id}
            style={styles.card}>
            <TouchableOpacity
                onPress={() => navigation.navigate('OneTrainingScreen', { route: training })}
            >
                <Text>TRAINING CART</Text>
                {/* <View style={styles.divInline}>
                    <Text style={styles.subtitles}>User:</Text>
                    <Text style={styles.description}>{publisher.username}</Text>
                </View>
                <View style={styles.divInline}>
                    <Text style={styles.subtitles}>Attempts:</Text>
                    <Text style={styles.description}>{attempts.toString()}</Text>
                </View>
                <View style={styles.divInline}>
                    <Text style={styles.subtitles}>Difficulty:</Text>
                    <Text style={styles.description}>{routeDifficultyLevel.name}</Text>
                </View>
                <View style={styles.divInline}>
                    <Text style={styles.subtitles}>Skill:</Text>
                    <Text style={styles.description}>{routeSkill.name}</Text>
                </View> */}
            </TouchableOpacity>

            <View style={styles.routeFooter}>
                <View style={styles.divFooter}>
                    <Icon
                        key={id}
                        name='heart-alt'
                        size={30}
                        color="#900"
                        onPress={() => {
                            // changeColor(id);
                            // handleOnClickLike(id)
                            // setCart(id)
                            // text === 'heart-alt' ? setText('heart') : setText('heart-alt')
                        }} />
                    <Icon
                        name='comment'
                        size={30}
                        color='black'
                    />
                </View>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 6,
        elevation: 3,
        backgroundColor: 'burlywood',
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        margin: 2,
        height: 200,
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
    },
    subtitles: {
        fontWeight: 'bold',
        textTransform: 'capitalize',
    },
    description: {
        fontWeight: 'normal',
    },
    divInline: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    routeFooter: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 50,
        backgroundColor: 'chartreuse'
    },
    divFooter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    container: {
        // flex:1,
        // justifyContent:'space-between'
    }
});

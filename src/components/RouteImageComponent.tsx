import React from 'react'
import { Image, StyleSheet, View } from 'react-native';
import { Image as Pic } from '../interfaces/models/models'

interface Props {
    image: number
}


export const RouteImageComponent = ({ image }: Props) => {

    const uri = `http://152.228.170.234:9001/image/${image}`;

    return (
        <View
            style={{
                width: 300,
                height: 420,
            }}
        >
            <View style={styles.imageContainer}>
                <Image
                    source={{ uri }}
                    style={styles.image}
                />
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        borderRadius: 18,
    },
    imageContainer: {
        flex: 1,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
})

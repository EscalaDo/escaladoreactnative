import { useEffect, useState } from 'react';
import { PermissionsAndroid, Platform } from 'react-native';


// export const checkReadExternalStoragePermission = async () => {
//     const result = await PermissionsAndroid.check(
//        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
//     );
//     return result;
//  };

export const checkReadExternalStoragePermission = async () => {

    const [result, setResult] = useState(false);
    // let result = false;
    useEffect(() => {
        async function fetchData() {
            try {
                const res = await PermissionsAndroid
                                .check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
                return res;
                                             
            } catch (err) {
                console.log(err);
            }

        }
        fetchData();
    }, []);
    // if(Platform.OS=="android"){
    //     const permissionAndroid = await PermissionsAndroid.check('android.permission.CAMERA');
    //     if(permissionAndroid != PermissionsAndroid.RESULTS.granted){
    //       const reqPer = await PermissionsAndroid.request('android.permission.CAMERA');
    //       if(reqPer != 'granted'){
    //         return false;
    //       }
    //     }
    //   }

    //result would be false if not granted and true if required permission is granted.
    // const result = PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
}


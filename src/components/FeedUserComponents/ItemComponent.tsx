import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useState } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Icon from "react-native-vector-icons/Fontisto"
import MIcon from "react-native-vector-icons/MaterialIcons"
import apiConstructor from '../../api/apiConstructor';
import { AuthContext } from '../../context/AuthContext';
import { Skill, Training } from '../../interfaces/models/modelAddRoutes';
import { Route, Publisher } from '../../interfaces/models/models';
import ShowCommentsFromRoute from '../../screens/ShowCommentsFromRoute';

// interface Props extends StackScreenProps<any, any> {
//     route: Route | Training,
// }


interface Props {
    route: Route,
    navigation: any,
    created?: boolean
}

const { width, height } = Dimensions.get('screen')

export const ItemComponent = ({ route, navigation, created }: Props) => {
    const {
        id,
        publisher,
        images,
        likes, comments } = route;

    const { user: currentUser } = useContext(AuthContext);

    const isFound = likes.some(({ user }) => {
        if (user.id === currentUser?.id) {
            return true;
        }

        return false;
    });

    const deleteItem = () => {
        console.log('deleted')
        const url = `http://152.228.170.234:9001/deleteRoute/${id}`
        apiConstructor
            .delete(url)
            .then(() => console.log('deleted'))
            .catch(console.log)
    }

    const [heartIcon, setHeartIcon] = useState<string>(isFound ? 'heart' : 'heart-alt');



    const onPress = async () => {
        await apiConstructor
            .post<boolean>(`http://152.228.170.234:9001/likeRoute/${id}/${currentUser?.id}`)
            .then(resp => {
                resp.data ?
                    setHeartIcon('heart') :
                    setHeartIcon('heart-alt')
            });
    }

    return (
        <View
            key={id}
            style={styles.container}
        >
            <TouchableOpacity
                key={id}
                onPress={() => {
                    navigation.navigate('OneRouteView', route)
                }}
            >
                <View
                    style={
                        styles.headerRoute
                    }>
                    <Text style={styles.textTitle}>
                        {
                            publisher.username
                        }
                        {
                            publisher.userRole.id === 2 ?
                                <MIcon name='verified-user' color='#1aa9c9' style={{ marginLeft: 10 }} /> :
                                <Text></Text>
                        }
                    </Text>
                    {
                        created ?
                            (
                                <TouchableOpacity
                                    style={styles.floatingButton}
                                >
                                    <Icon
                                        name={'trash'}
                                        size={20} color='red'
                                        onPress={deleteItem} />
                                </TouchableOpacity>
                            ) :
                            console.log('no es su ruta')
                    }
                </View>
                <View style={styles.body}>
                    {
                        (<View>
                            <View>
                                <Image style={{ height: width / 2.01, width: width / 1.01 }} source={{ uri: `http://152.228.170.234:9001/image/${images[0].id}` }} />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                                <Icon
                                    name={heartIcon}
                                    size={20} color='#D45B12'
                                    onPress={onPress} />
                                <Text style={{ color: 'black' }}>   {likes.length}</Text>
                                <Icon name='comment' size={20} color='#D45B12' style={{ marginLeft: 10 }} onPress={() => {
                                    // console.log(id)
                                    navigation.navigate('CommentsScreen', { idRoute: id })
                                }} />
                                <Text style={{ color: 'black' }}>  {comments.length}</Text>
                            </View>

                        </View>)
                        // (<Text>ESTO ES UNA TRAINING</Text>)
                    }
                </View>
            </TouchableOpacity>
        </View>
    )
}



// const isRoute = (route: Route | Training) => {
//     return (route as Route).attempts !== undefined
// }

const styles = StyleSheet.create({
    container: {
        width: width,
        height: height / 2.8,
        // backgroundColor: 'white'

    },
    headerTraining: {
        backgroundColor: 'red'
    },
    headerRoute: {
        // backgroundColor: 'white',
        height: 50,
        alignItems: 'stretch',
        justifyContent: 'center'
    },
    body: {

    },
    textTitle: {
        fontWeight: 'bold',
        color: 'black',
        margin: 10
    },
    floatingButton: {
        width: 60,
        height: 60,
        borderRadius: 30,
        position: 'absolute',
        top: 10,
        right: 0,
    }
});

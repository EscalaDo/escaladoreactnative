import { StackScreenProps } from '@react-navigation/stack';
import React from 'react'
import { Dimensions, StyleSheet, Text, View } from 'react-native'
import { Comment } from '../../interfaces/models/models';


const { width, height } = Dimensions.get('screen');

interface Props {
    route: Comment,
    navigation: any,
}

export const CommentItem = ({ route, navigation }: Props) => {

    const { id, description, user, timestamp } = route;

    return (
        <View style={styles.container}>
            <Text style={{fontWeight:'bold', fontSize: 18}}>{user.username}</Text>
            <Text>{description}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderColor: '#cacccb',
        borderWidth: 1,
        width: width,
        height: width / 4,
        flexDirection: 'column',
        padding: 10,
    }
})

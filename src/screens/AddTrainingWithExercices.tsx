import React, { MutableRefObject, useContext, useRef, useState } from 'react'
import { Alert, Pressable, ScrollView, StyleSheet, Text, TextInput, ToastAndroid, View, TouchableOpacity, FlatList, Dimensions } from 'react-native';
import InputSpinner from 'react-native-input-spinner';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/Entypo';


import { AuthContext } from '../context/AuthContext';
import { User } from '../interfaces/models/modelAddRoutes';
import apiConstructor from '../api/apiConstructor';

let data = new FormData();
const { height, width } = Dimensions.get('screen')
export const AddTrainingWithExercices = () => {
    const { user } = useContext(AuthContext);

    // value of the training name.
    const [trainingName, setTrainingName] = useState<string>('');
    // this will be attached with each input onChangeText
    const [textValue, setTextValue] = useState('');
    const [numSets, setNumSets] = useState(1);
    const [numReps, setNumReps] = useState(1);
    // our number of inputs, we can add the length or decrease the length
    const [numInputs, setNumInputs] = useState(1);
    // all our input fields are tracked with this array
    const refInputsNames = useRef<string[]>([textValue]);
    const refInputsSets = useRef<number[]>([numSets]);
    const refInputsReps = useRef<number[]>([numReps]);


    const setInputValueName = (index: number, value: string) => {
        // first, we are storing input value to refInputs array to track them
        const inputs = refInputsNames.current;
        inputs[index] = value;
        // we are also setting the text value to the input field onChangeText
        setTextValue(value);
    }
    const setInputValueSets = (index: number, value: number) => {
        // first, we are storing input value to refInputs array to track them
        const inputs = refInputsSets.current;
        inputs[index] = value;
        // we are also setting the text value to the input field onChangeText
        setNumSets(value);
    }
    const setInputValueReps = (index: number, value: number) => {
        // first, we are storing input value to refInputs array to track them
        const inputs = refInputsReps.current;
        inputs[index] = value;
        // we are also setting the text value to the input field onChangeText
        setNumReps(value);
    }

    const resetAllForm = () => {
        setNumInputs(1)
        for (let i = 0; i < numInputs; i++) {
            if (i !== 0) {
                refInputsNames.current.splice(i, 1)[0];
                refInputsSets.current.splice(i, 1)[0];
                refInputsReps.current.splice(i, 1)[0];
            }
        }
        refInputsNames.current[0] = '';
        refInputsSets.current[0] = 1;
        refInputsReps.current[0] = 1;
        setTrainingName('')
        setNumSets(1)
        setNumReps(1)
    }

    const addInput = () => {
        // add a new element in our refInputs array
        refInputsNames.current.push('');
        refInputsSets.current.push(1);
        refInputsReps.current.push(1);
        // increase the number of inputs
        setNumInputs(value => value + 1);
    }
    const removeInput = (i: number) => {
        // remove from the array by index value
        if (refInputsNames.current.length > 1) {
            refInputsNames.current.splice(i, 1)[0];
            refInputsSets.current.splice(i, 1)[0];
            refInputsReps.current.splice(i, 1)[0];
            // decrease the number of inputs
            setNumInputs(value => value - 1);
        }
        else {
            Toast.show("Must have one exercise atleast!", Toast.SHORT);
            //console.log("Reach here");
        }
    }
    const upload = () => {
        if (!refInputsNames.current.includes('') && !(trainingName === "")) {
            refInputsNames.current.map((value, i) => {
                data.append("nameEx", value);
                console.log((i + 1) + ": ", value);
            });
            refInputsSets.current.map((value, i) => {
                data.append("sets", value);
                console.log((i + 1) + ": ", value);
            });
            refInputsReps.current.map((value, i) => {
                data.append("reps", value);
                console.log((i + 1) + ": ", value);
            });
            data.append("userId", user?.id);
            data.append("name", trainingName);

            apiConstructor.post('http://152.228.170.234:9001/createTraining', data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            })
                .then(response => {
                    Alert.alert(
                        "Success",
                        "Your training has been created",
                        [
                            {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                            },
                            {
                                text: "OK",
                                onPress: resetAllForm,
                            }
                        ]
                    );
                }).catch(err => {
                    console.log(err)
                })
        } else {
            Alert.alert("Woops!", "All exercises and training must have a name!")
        }

        data = new FormData();

    }


    const inputs: JSX.Element[] = [];
    for (let i = 0; i < numInputs; i++) {
        inputs.push(
            <View key={i} style={styles.exercisesContainer}>
                <TextInput
                    // textAlign='center'
                    // style={styles.exercisesInputName}
                    style={{ bottom: 20 }}
                    underlineColorAndroid={'#F3872F'}
                    onChangeText={value => setInputValueName(i, value)}
                    value={refInputsNames.current[i]}
                    placeholder="Exercice Name"
                />
                <View>
                    <Text>Sets</Text>
                    <InputSpinner
                        style={styles.spinners}
                        max={99}
                        min={1}
                        step={1}
                        value={refInputsSets.current[i]}
                        colorLeft="#999999"
                        colorRight="#999999"
                        colorMax={"#f04048"}
                        colorMin={"#40c5f4"}
                        onDecrease={() => {
                            if (numSets > 1) {
                                setInputValueSets(i, refInputsSets.current[i] - 1);
                            } else {
                                Alert.alert("Error", "Number of attempts cannot be lower than 1!")
                            }
                        }}
                        onIncrease={() => {
                            if (numSets < 99) {
                                setInputValueSets(i, refInputsSets.current[i] + 1);
                            } else {
                                Alert.alert("Error", "Number of attempts cannot be greater than 99!")
                            }
                        }}
                    />
                    <Text>Reps</Text>
                    <InputSpinner
                        style={styles.spinners}
                        max={99}
                        min={1}
                        step={1}
                        colorLeft="#999999"
                        colorRight="#999999"
                        value={refInputsReps.current[i]}
                        colorMax={"#f04048"}
                        colorMin={"#40c5f4"}
                        onDecrease={() => {
                            if (numReps > 1) {
                                setInputValueReps(i, refInputsReps.current[i] - 1);
                            } else {
                                Alert.alert("Error", "Number of attempts cannot be lower than 1!")
                            }
                        }}
                        onIncrease={() => {
                            if (numReps < 99) {
                                setInputValueReps(i, refInputsReps.current[i] + 1);
                            } else {
                                Alert.alert("Error", "Number of attempts cannot be greater than 99!")
                            }
                        }}
                    />
                </View>
                {/* To remove the input */}
                <Pressable onPress={() => removeInput(i)} style={{ flexDirection: 'row', justifyContent: 'center', alignContent: 'center', top: 10 }}>
                    <Icon name={'trash'} size={20} color={'#F3872F'} />
                    <Text style={{ textAlign: "center" }}>Delete Exercise</Text>
                </Pressable>
            </View>
        );
    }


    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#F3872F',
                padding: 20,
                // justifyContent:'space-between'
            }}
        >
            <View style={{
                backgroundColor: 'white',
                height: height / 8,
                // margin: 10,
                // marginHorizontal: 20,
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 12,
                },
                shadowOpacity: 0.58,
                shadowRadius: 16.00,

                elevation: 24,
                padding: 30,
            }}>
                <View
                    style={{
                        // padding: 50,
                    }}
                >
                    <Text>Training name:</Text>
                    <TextInput
                        // style={styles.inputTrainingName}
                        // textAlign='center'
                        underlineColorAndroid={'#F3872F'}
                        value={trainingName}
                        onChangeText={(value) => setTrainingName(value)}
                        placeholder="Enter training name..."
                    />
                </View>

            </View>
            <View style={{
                // backgroundColor: 'white',
                height: height / 1.8,
                top: 10
            }}>
                <ScrollView>
                    {inputs}
                </ScrollView>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    top: 40,
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}
            >
                <TouchableOpacity
                    style={{
                        backgroundColor: 'white',
                        padding: 15,
                        borderRadius: 10,
                        flexDirection: 'row-reverse',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                    onPress={addInput}
                >
                    <Text style={{ fontWeight: 'bold' }}>Add exercise!</Text>
                    <Icon name='md-add-circle-sharp' size={30} color='#F3872F' />
                </TouchableOpacity>

                <TouchableOpacity
                    style={{
                        backgroundColor: 'white',
                        padding: 15,
                        borderRadius: 10,
                        flexDirection: 'row-reverse',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                    onPress={upload}
                >
                    <Text style={{ fontWeight: 'bold' }}>Save Training!</Text>
                    <Icon2 name='save' size={25} color='#F3872F' />
                </TouchableOpacity>
            </View>
        </View >

        // <ScrollView contentContainerStyle={styles.container}>
        //     <View>
        //         {/* <Text>
        //             Training name:
        //         </Text> */}
        //         <TextInput
        //             style={styles.inputTrainingName}
        //             textAlign='center'
        //             value={trainingName}
        //             onChangeText={(value) => setTrainingName(value)}
        //             placeholder="Enter training name..."
        //         />
        //     </View>
        //     {inputs}
        //     {/* Button to add extra exercises to training if required */}
        //     <TouchableOpacity style={styles.buttonAddEx} onPress={addInput}>
        //         <Text style={styles.textAddEx}>Add exercise!</Text>
        //     </TouchableOpacity>
        //     <Text style={{ textAlign: "center" }}>OR</Text>
        //     {/* Send data from exercises and training to spring boot*/}
        //     <TouchableOpacity
        //         style={styles.buttonSend}
        //         onPress={upload}
        //     >
        //         <Text style={styles.textSend}>Save Training!</Text>
        //     </TouchableOpacity>
        // </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {

        paddingTop: 30,
        // backgroundColor: "white",
        padding: 1,
    },
    //button to send route to Spring boot
    buttonSend: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'black',
    },
    textSend: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
    },
    //container for each exercise added
    exercisesContainer: {
        backgroundColor: "white",
        borderColor: "gray",
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        padding: 40,
        marginTop: 10,
        marginBottom: 10
    },
    exercisesInputName: {
        backgroundColor: "white",
        borderColor: "gray",
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        marginBottom: 2
    },
    //input for the training name
    inputTrainingName: {
        // borderColor: "gray",
        // width: "100%",
        // borderWidth: 1,
        // borderRadius: 10,
        // padding: 10,
        // borderBottomWidth: 1,
        backgroundColor: "white",
        borderRadius: 10

    },
    //button to add exercise to training
    buttonAddEx: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 12,
        paddingHorizontal: 32,
        borderRadius: 4,
        elevation: 3,
        backgroundColor: 'purple',
    },
    textAddEx: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'black',
    },
    //spinner styles
    spinners: {
        backgroundColor: "white",
        marginTop: 1,
        marginBottom: 2
    },
});
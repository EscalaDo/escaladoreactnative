import React, { useEffect, useState } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { getRoutes } from "../hooks/GETS/getAllRoutesHook";
import Icon from "react-native-vector-icons/Fontisto"
import Icon2 from "react-native-vector-icons/MaterialIcons"
import { useNavigation, NavigationContainer } from '@react-navigation/native';
import { LoginOrSignUp } from './LoginOrSignUp';
import { StackScreenProps } from "@react-navigation/stack";
import { RouteCard } from "../components/cards/RouteCard";
import { Route } from '../interfaces/models/models';


interface Props extends StackScreenProps<any, any> {
    idUser?: number;
}


export const AllRoutes = ({ navigation, idUser, route }: Props) => {
    let routes: Route[] = [];

    const fillRoute = (obj: boolean) => {

        !obj ?
            getRoutes().then(x => {
                x.map(xx => routes.push(xx))
            })
            :
            getRoutes(idUser).then(x => {
                x.map(xx => routes.push(xx))
            })
    }

    fillRoute(false);


    return (
        <View>
            <ScrollView>
                {
                    routes.map((item) => (
                        <RouteCard route={item} navigation={navigation} key={item.id} />
                    ))
                }
            </ScrollView>

            <TouchableOpacity
                style={styles.buttonFloating}
                // onPress={() => { navigation.navigate('AddNewRouteView') }}
                onPress={() => { navigation.navigate('UserFeedScreen') }}
            >
                <Icon name='plus-a' size={30} color='#01a699' />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    buttonFloating: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 70,
        backgroundColor: '#fff',
        borderRadius: 100,
    }
})


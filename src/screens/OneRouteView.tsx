import { View, Text, Dimensions, StyleSheet, TouchableOpacity } from 'react-native';
import React, { useState } from 'react'
import { getOneRoute } from '../hooks/GETS/getOneRouteHook';
import { StackScreenProps } from '@react-navigation/stack';
import Carousel from 'react-native-snap-carousel';
import { RouteImageComponent } from '../components/RouteImageComponent';
import Icon from 'react-native-vector-icons/Fontisto';
import { Route } from '../interfaces/models/models';

interface Props extends StackScreenProps<any, any> { };

const { width: windowWidth } = Dimensions.get('window');

export const OneRoute = ({ route, navigation }: Props) => {

  const ruta = route.params as Route;

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Carousel
          data={ruta.images.map(x => x.id)}
          renderItem={({ item }: any) => <RouteImageComponent image={item} />}
          sliderWidth={350}
          itemWidth={300}
        />
      </View>
      <View style={styles.body}>
        <Text>Attemps: {ruta.attempts}</Text>
        <Text>Difficult level: {ruta.routeDifficultyLevel.name}</Text>
        <Text>RoutegripType: {ruta.routeGripType.name}</Text>
      </View>
      <TouchableOpacity
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          width: 50,
          position: 'absolute',
          top: 1,
          left: 1,
          height: 50,
        }}
        onPress={() => { navigation.goBack() }}
      >
        <Icon name='arrow-left' size={30} color='black' />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    // backgroundColor: '#1a1918',
    top: 55,
    alignContent: 'center',
  },
  body: {
    top: 100,
    backgroundColor: '#f2cc8a',
    padding: 20,
    margin: 20,
    borderRadius: 10,
  }

});
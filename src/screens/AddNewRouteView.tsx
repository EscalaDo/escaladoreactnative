import React, { useContext, useEffect, useState } from "react";
import { StyleSheet, Text, TextInput, View, Alert, TouchableOpacity, Pressable, Dimensions } from 'react-native';
import ImagePicker, { Image } from 'react-native-image-crop-picker';
import { Grade, Grip, Skill } from '../interfaces/models/modelAddRoutes';
import { getSkills } from '../hooks/GETS/getSkillsHook';
import { getGrades } from '../hooks/GETS/getGradesHook';
import { getGrips } from '../hooks/GETS/getGripsHook';
import axios from 'axios';
import InputSpinner from "react-native-input-spinner";
import { Picker } from "@react-native-picker/picker";
import { StackNavigationProp, StackScreenProps } from "@react-navigation/stack";
import { AuthContext } from '../context/AuthContext';
import apiConstructor from "../api/apiConstructor";
import { ClimbingGym } from "../interfaces/models/models";
import { getClimbingGyms } from "../hooks/GETS/getAllClimbingGymHook";
import Icon from "react-native-vector-icons/AntDesign"



interface Props extends StackScreenProps<any, any> { }

let data = new FormData();
let hasPics = false;

const { height, width } = Dimensions.get('screen')

export const AddNewRouteView = ({ navigation }: Props) => {

    const [useDisabled, setUseDisabled] = useState(false)

    const { user } = useContext(AuthContext)
    const skills: Skill[] = getSkills();
    const [selectedValueSkill, setSelectedValueSkill] = useState<number>(1);

    const grades: Grade[] = getGrades();
    const [selectedValueGrade, setSelectedValueGrade] = useState<number>(1);

    const grips: Grip[] = getGrips();
    const [selectedValueGrip, setSelectedValueGrip] = useState<number>(1);

    const [descriptionText, setDescriptionText] = useState('');
    const [numAttempts, setNumAttempts] = useState<number>(1);

    const [imagesArray, setImagesArray] = useState<Image[]>([]);

    const gym: ClimbingGym[] = getClimbingGyms();
    const [selectedValueClimbingGym, setSelectedValueClimbingGym] = useState<number>(1);

    const pickFromGallery = () => {
        ImagePicker.openPicker({
            multiple: true,
            maxFiles: 5,
            mediaType: 'photo',
        }).then(images => {
            setImagesArray(images);
        });
    }

    const validateForm = () => {

        if (imagesArray.length === 0) return { state: false, desc: 'No images selected' }

        if (descriptionText === '') return { state: false, desc: 'No title added' }

        if (selectedValueSkill === 0) setSelectedValueSkill(1)
        if (selectedValueGrip === 0) setSelectedValueGrip(1)
        if (selectedValueClimbingGym === 0) setSelectedValueClimbingGym(1)
        if (selectedValueGrade === 0) setSelectedValueGrade(1)

        return {
            state: true,
            desc: 'Route added correctly'
        }
    }

    const errorOnSend = () => {
        setUseDisabled(false)
        Alert.alert('Please, fill all fields')
    }

    const resetForm = () => {
        setSelectedValueSkill(1)
        setSelectedValueGrade(1)
        setSelectedValueGrip(1)
        setDescriptionText('')
        setNumAttempts(1)
        setImagesArray([])
        setUseDisabled(false)
    }
    const upload = (
        imagesArray: Image[],
        skillId: number,
        gripTypeId: number,
        difficultyLevelId: number,
        description: string,
        attempts: number,
        userId: number | undefined,
        navigation: StackNavigationProp<any, any, undefined>,
        gymId: number) => {
        let cont = 0;

        imagesArray.forEach(x => data.append("files", { uri: x.path, name: `image${cont++}.jpg`, type: x.mime }));
        data.append("userId", userId);
        data.append("skillId", skillId);
        data.append("gripTypeId", gripTypeId);
        data.append("difficultyLevelId", difficultyLevelId);
        data.append("description", description);
        data.append("attempts", attempts);
        data.append("climbingGymId", gymId);
        console.log('LO QUE S EENVIA-------------', data);

        apiConstructor
            .post('http://152.228.170.234:9001/uploadRoute', data, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            })
            .then(() => {
                Alert.alert(
                    "Route Created",
                    "Added route succesfully",
                    [
                        {
                            text: "Create new route",
                            onPress: resetForm,
                            style: "cancel"
                        },
                        {
                            text: "Continue", onPress: () => {
                                resetForm();
                                navigation.goBack()
                            }
                        }
                    ]
                );
            })
            .catch(err => console.log(err))
    }

    return (

        <View style={styles.containerPadre}>
            <View style={styles.body}>
                <View style={{paddingBottom: 20}}>
                    <Text>Route description:</Text>
                    <TextInput
                        placeholder="Add route description..."
                        underlineColorAndroid={'#F3872F'}
                        value={descriptionText}
                        onChangeText={newText => setDescriptionText(newText)}
                    />
                </View>
                <View style={styles.rowItem}>
                    <Text>Grade:</Text>
                    <Picker
                        style={{ width: 200 }}
                        mode="dropdown"
                        selectedValue={selectedValueGrade}
                        onValueChange={(itemValue: number, itemIndex) => {
                            setSelectedValueGrade(itemValue);
                            console.log(itemValue);
                        }}
                    >
                        {grades.length !== 0 ? (
                            grades.map(item => {
                                return <Picker.Item label={item.name} value={item.id} key={item.id} />;
                            })
                        ) : (
                            <Picker.Item label="Loading..." value="0" />
                        )}
                    </Picker>
                </View>
                <View style={styles.rowItem}>
                    <Text>Gryp type:</Text>
                    <Picker
                        style={{ width: 200 }}
                        mode="dropdown"
                        selectedValue={selectedValueGrip}
                        onValueChange={(itemValue: number, itemIndex) => {
                            setSelectedValueGrip(itemValue);
                            console.log(itemValue);
                        }}
                    >
                        {grips.length !== 0 ? (
                            grips.map(item => {
                                return <Picker.Item label={item.name} value={item.id} key={item.id} />;
                            })
                        ) : (
                            <Picker.Item label="Loading..." value="0" />
                        )}
                    </Picker>
                </View>
                <View style={styles.rowItem}>
                    <Text>Skill:</Text>
                    <Picker
                        style={{ width: 200 }}
                        mode="dropdown"
                        selectedValue={selectedValueSkill}
                        onValueChange={(itemValue: number, itemIndex) => {
                            setSelectedValueSkill(itemValue)
                            console.log(itemValue);
                        }}
                    >
                        {skills.length !== 0 ? (
                            skills.map(item => {
                                return <Picker.Item label={item.name} value={item.id} key={item.id} />;
                            })
                        ) : (
                            <Picker.Item label="Loading..." value="0" />
                        )}
                    </Picker>
                </View>
                <View style={styles.rowItem}>
                    <Text>Climbing gym:</Text>
                    <Picker
                        style={{ width: 200 }}
                        mode="dropdown"
                        selectedValue={selectedValueClimbingGym}
                        onValueChange={(itemValue: number, itemIndex) => {
                            setSelectedValueClimbingGym(itemValue)
                        }}
                    >
                        {gym.length !== 0 ? (
                            gym.map(item => {
                                return <Picker.Item label={item.name} value={item.id} key={item.id} />;
                            })
                        ) : (
                            <Picker.Item label="Loading..." value="0" />
                        )}
                    </Picker>
                </View>
                <View style={styles.rowItem}>
                    <Text>Attempts:</Text>
                    <InputSpinner
                        max={99}
                        min={1}
                        step={1}
                        value={numAttempts}
                        style={{ width: 200 }}
                        colorLeft="#999999"
                        colorRight="#999999"
                        colorMax={"#f04048"}
                        colorMin={"#40c5f4"}
                        onDecrease={() => {
                            if (numAttempts > 1) {
                                setNumAttempts(numAttempts - 1);
                            } else {
                                Alert.alert("Error", "Number of attempts cannot be lower than 1!")
                            }
                        }}
                        onIncrease={() => {
                            if (numAttempts < 99) {
                                setNumAttempts(numAttempts + 1);
                            } else {
                                Alert.alert("Error", "Number of attempts cannot be greater than 99!")
                            }
                        }}
                    />

                </View>
                <View style={[styles.rowItem, {top: 10}]}>
                    <TouchableOpacity onPress={pickFromGallery} style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                        <Icon name='upload' size={30} color='#F3872F' />

                        <Text style={{ marginLeft: 10 }}>Upload images</Text>
                    </TouchableOpacity>
                    <Text>{imagesArray.length}</Text>
                </View>
                <View style={{top: 40, alignItems: 'center'}}>
                    <TouchableOpacity
                        disabled={useDisabled}
                        style={{
                            backgroundColor: '#F3872F',
                            padding: 20,
                            borderRadius: 10
                        }}
                        onPress={
                            () => {
                                let { state, desc } = validateForm();
                                setUseDisabled(true)
                                state ?
                                    upload(imagesArray, selectedValueSkill, selectedValueGrip, selectedValueGrade, descriptionText, numAttempts, user?.id, navigation, selectedValueClimbingGym) :
                                    errorOnSend()
                            }
                        }
                    >
                        <Text style={{ color: 'white', fontWeight: 'bold' }}>Add route</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View >
    )
}



const styles = StyleSheet.create({
    containerPadre: {
        flex: 1,
        backgroundColor: '#F3872F',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    body: {
        backgroundColor: 'white',
        height: height / 1.5,
        width: width - 20,
        borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        flexDirection: 'column',
        padding: 50,
    },
    rowItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    }
});

import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect, useState } from 'react'
import { AuthContext } from '../../../context/AuthContext'
import { TrainingFeedScreen } from '../../TrainingScreens/TrainingFeedScreen'


interface Props extends StackScreenProps<any, any> { }


export const MyTrainingScreen = ({ route, navigation }: Props) => {

    const { user } = useContext(AuthContext)
    const [crtd, setCrtd] = useState(route.params?.created)


    useEffect(() => {
        setCrtd(route.params?.created)
    }, [route.params?.created])

    return (
        <TrainingFeedScreen navigation={navigation} route={route} userId={user?.id} created={crtd} key={user?.id} />
    )
}


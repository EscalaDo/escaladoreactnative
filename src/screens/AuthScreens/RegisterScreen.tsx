import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useState } from 'react'
import { View, Text, KeyboardAvoidingView, Platform, StyleSheet, TextInput, TouchableOpacity, Keyboard, Dimensions } from 'react-native';
import { onChange } from 'react-native-reanimated'
import { Background } from '../../components/UserAuth/Background'
import { useForm } from '../../hooks/FormHooks/useForm'
import { AuthContext } from '../../context/AuthContext';
import { getRoles } from '../../hooks/GETS/getRolesHook'
import { Role } from '../../interfaces/models/models'
import { Picker } from '@react-native-picker/picker'
import { ScrollView } from 'react-native-gesture-handler'


interface Props extends StackScreenProps<any, any> { };

const { height } = Dimensions.get('screen')
export const RegisterScreen = ({ navigation }: Props) => {

    const { signUp } = useContext(AuthContext)
    const roles: Role[] = getRoles();
    const [role, setRole] = useState<number>(1);

    const { username, password, name, lastname, email, roleId, location, onChange } = useForm({
        username: '',
        password: '',
        name: '',
        lastname: '',
        email: '',
        roleId: role,
        location: '',

    });

    const onRegister = () => {
        console.log({ username, password, name, lastname, email, roleId, location })
        Keyboard.dismiss()
        signUp({
            name: name,
            lastname: lastname,
            email: email,
            username: username,
            password: password,
            roleId: roleId,
            location: location
        })
    }

    return (
        <>
            <KeyboardAvoidingView
                style={{ flex: 1, backgroundColor: '#F3872F' }}
                behavior={(Platform.OS === 'ios') ? 'padding' : 'height'}
            >
                <ScrollView>
                    <View style={style.formContainer}>
                        <Text style={style.title}>Register</Text>

                        <Text style={style.label}>Name:</Text>
                        <TextInput
                            placeholder='Insert name'
                            placeholderTextColor='rgba(255,255,255,0.4)'
                            style={style.inputstyle}
                            underlineColorAndroid={'white'}
                            onChangeText={(value) => onChange(value, 'name')}
                            value={name}
                            autoCapitalize='words'
                            onSubmitEditing={onRegister}
                        />

                        <Text style={style.label}>Lastname:</Text>
                        <TextInput
                            placeholder='Insert lastname'
                            style={style.inputstyle}
                            placeholderTextColor='rgba(255,255,255,0.4)'
                            underlineColorAndroid={'white'}
                            onChangeText={(value) => onChange(value, 'lastname')}
                            value={lastname}
                            autoCapitalize='words'
                            onSubmitEditing={onRegister}
                        />

                        <Text style={style.label}>Email:</Text>
                        <TextInput
                            placeholder='Insert email'
                            style={style.inputstyle}
                            placeholderTextColor='rgba(255,255,255,0.4)'
                            keyboardType='email-address'
                            underlineColorAndroid={'white'}
                            onChangeText={(value) => onChange(value, 'email')}
                            value={email}
                            autoCapitalize='none'
                            onSubmitEditing={onRegister}
                        />

                        <Text style={style.label}>Username:</Text>
                        <TextInput
                            placeholder='Insert username'
                            style={style.inputstyle}
                            placeholderTextColor='rgba(255,255,255,0.4)'
                            underlineColorAndroid={'white'}
                            onChangeText={(value) => onChange(value, 'username')}
                            value={username}
                            autoCapitalize='none'
                            autoCorrect={false}
                            onSubmitEditing={onRegister}
                        />

                        <Text style={style.label}>Password:</Text>
                        <TextInput
                            secureTextEntry
                            placeholder='*******'
                            style={style.inputstyle}
                            placeholderTextColor='rgba(255,255,255,0.4)'
                            underlineColorAndroid={'white'}
                            onChangeText={(value) => onChange(value, 'password')}
                            value={password}
                            autoCapitalize='none'
                            autoCorrect={false}
                            onSubmitEditing={onRegister}
                        />
                        <Text style={style.label}>Role:</Text>
                        <View>
                            <Picker
                                style={style.picker}
                                mode="dropdown"
                                selectedValue={role}
                                onValueChange={(itemValue: number) => {
                                    setRole(itemValue);
                                    onChange(itemValue, 'roleId');
                                    console.log(itemValue);
                                }}
                            >
                                {roles.length !== 0 ? (
                                    roles.map(item => {
                                        return <Picker.Item label={item.name} value={item.id} key={item.id}
                                        />;
                                    })
                                ) : (
                                    <Picker.Item label="Loading..." value="0" />
                                )}
                            </Picker>
                            {
                                role == 2 ? (
                                    <View>
                                        <Text style={style.label}>Gym Location:</Text>
                                        <TextInput
                                            placeholder='City'
                                            placeholderTextColor='rgba(255,255,255,0.4)'
                                            underlineColorAndroid={'white'}
                                            style={style.inputstyle}
                                            onChangeText={(value) => onChange(value, 'location')}
                                            value={location}
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            onSubmitEditing={onRegister}
                                        />
                                    </View>

                                )

                                    :
                                    <View></View>
                            }
                        </View>
                        {/* Boton login */}
                        <View style={style.buttonContainer}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={style.button}
                                onPress={onRegister}
                            >
                                <Text style={style.buttonLabel}>Register</Text>
                            </TouchableOpacity>
                        </View>

                        {/* Crear nueva cuenta */}
                        <View style={style.newUserContainer}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                // style={style.button}
                                onPress={() => { navigation.navigate('LoginScreen') }}
                            >
                                <Text style={style.buttonLabelToLogin}>Login screen</Text>

                            </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </>
    )
}


const style = StyleSheet.create({
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 20
    },
    label: {
        color: 'white',
        fontWeight: 'bold',
        marginTop: 25
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 50
    },
    button: {
        borderWidth: 2,
        borderColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 5,
        borderRadius: 100,
        bottom: 10
    },
    buttonLabel: {
        fontSize: 18,
        color: 'white',

    },
    newUserContainer: {
        alignItems: 'flex-end',
        marginTop: 10,
        bottom: 20
    },
    formContainer: {
        flex: 1,
        paddingHorizontal: 30,
        justifyContent: 'center',
        height: height,
        marginBottom: 50

    },
    buttonLabelToLogin: {
        fontSize: 18,
        color: 'white',
        bottom: 10
    },
    picker: {
        width: "100%",
        color: 'white'
    },
    inputstyle: {
        color: 'white'
    }
})
import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect, useState } from 'react'
import { Text, View } from 'react-native'
import { AllRoutes } from '../../AllRoutesView'
import { UserFeedScreen } from '../../UserFeedScreens/UserFeedScreen';
import { AuthContext } from '../../../context/AuthContext';



interface Props extends StackScreenProps<any, any> { }


export const MyRoutesScreen = ({ route, navigation }: Props) => {
  const { user } = useContext(AuthContext)
  const [crtd, setCrtd] = useState(route.params?.created)

  useEffect(() => {
    setCrtd(route.params?.created)
  }, [route.params?.created])



  return (
    <UserFeedScreen navigation={navigation} route={route} userId={user?.id} created={crtd} key={user?.id} />
  )
}

import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useEffect } from 'react'
import { Text, TextInput, View, TouchableOpacity, StyleSheet, KeyboardAvoidingViewBase, KeyboardAvoidingView, Platform, Keyboard, Alert, Dimensions } from 'react-native';
import { Background } from '../../components/UserAuth/Background'
import { AuthContext } from '../../context/AuthContext';
import { useForm } from '../../hooks/FormHooks/useForm';


interface Props extends StackScreenProps<any, any> { };


const {height} = Dimensions.get('screen')

export const LoginScreen = ({ navigation }: Props) => {

    const { signIn, errorMessage, removeError } = useContext(AuthContext)


    const { username, password, onChange } = useForm({
        username: '',
        password: '',
    });


    useEffect(() => {
        if (errorMessage.length === 0) return;

        Alert.alert('Login incorrecto', errorMessage, [{ text: 'Ok', onPress: removeError }]);
    }, [errorMessage])


    const onLogin = () => {
        console.log({ username, password })

        Keyboard.dismiss()

        signIn({ username, password });
    }

    return (
        <>
            <Background />
            <KeyboardAvoidingView
                style={{ flex: 1, height:height,backgroundColor: '#F3872F' }}
                behavior={(Platform.OS === 'ios') ? 'padding' : 'height'}
            >
                <View style={style.formContainer}>
                    <Text style={style.title}>Login</Text>
                    <Text style={style.label}>Username:</Text>
                    <TextInput
                        placeholder='Insert username'
                        placeholderTextColor='rgba(255,255,255,0.4)'
                        style={style.inputstyle}
                        underlineColorAndroid={'white'}
                        onChangeText={(value) => onChange(value, 'username')}
                        value={username}
                        autoCapitalize='none'
                        autoCorrect={false}
                        onSubmitEditing={onLogin}
                    />

                    <Text style={style.label}>Password:</Text>
                    <TextInput
                        secureTextEntry
                        placeholder='*******'
                        style={style.inputstyle}
                        placeholderTextColor='rgba(255,255,255,0.4)'
                        underlineColorAndroid={'white'}
                        onChangeText={(value) => onChange(value, 'password')}
                        value={password}
                        autoCapitalize='none'
                        autoCorrect={false}
                        onSubmitEditing={onLogin}
                    />
                    {/* Boton login */}
                    <View style={style.buttonContainer}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            style={style.button}
                            onPress={onLogin}
                        >
                            <Text style={style.buttonLabel}>Sign in</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Crear nueva cuenta */}
                    <View style={style.newUserContainer}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            // style={style.button}
                            onPress={() => { navigation.navigate('RegisterScreen') }}
                        >
                            <Text style={style.buttonLabel}>New account</Text>

                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </>
    )
}


const style = StyleSheet.create({
    title: {
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 20
    },
    label: {
        color: 'white',
        fontWeight: 'bold',
        marginTop: 25
    },
    buttonContainer: {
        alignItems: 'center',
        marginTop: 50
    },
    button: {
        borderWidth: 2,
        borderColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 5,
        borderRadius: 100
    },
    buttonLabel: {
        fontSize: 18,
        color: 'white'
    },
    newUserContainer: {
        alignItems: 'flex-end',
        marginTop: 10
    },
    formContainer: {
        flex: 1,
        backgroundColor:'#F3872F',
        paddingHorizontal: 30,
        justifyContent: 'center',
        height: 600,
        marginBottom: 50
    },
    inputstyle: {
        color: 'white'
    }
})
import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect, useState } from 'react'
import { ScrollView, Text, RefreshControl, View, TouchableOpacity, StyleSheet } from 'react-native';
import apiConstructor from '../../api/apiConstructor'
import { Training } from '../../interfaces/models/modelAddRoutes'
import { LoadingScreen } from '../AuthScreens/LoadingScreen'
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons"
import { FlatList } from 'react-native-gesture-handler';
import { ItemComponent } from '../../components/TrainingFeedComponent/ItemComponent';
import Icon from "react-native-vector-icons/Fontisto"
import { AuthContext } from '../../context/AuthContext';


interface Props extends StackScreenProps<any, any> {
  userId?: number;
  created?: boolean;
}

export const TrainingFeedScreen = ({ navigation, userId, created }: Props) => {


  const {user} = useContext(AuthContext) 

  const [trainings, setTrainings] = useState<Training[]>();
  const [refresh, setRefresh] = useState(false);

  const fillListTrainings = (obj: Training[]) => {
    setTimeout(() => {
      setTrainings(obj)
      setRefresh(false)
    }, 500);
  }

  const onRefresh = () => {
    setRefresh(true);

    let url = '';


    if (typeof userId !== 'number') {
      url = 'http://152.228.170.234:9001/getAllTrainings';
    }
    else {
      if (!created) {
        url = `http://152.228.170.234:9001/getUserLikedTrainings/${userId}`
      } else {
        url = `http://152.228.170.234:9001/getUserCreatedTrainings/${userId}`
      }
    }

    const resp = apiConstructor
      .get<Training[]>(url)
      .then((x) => {
        fillListTrainings(x.data)
      })

    };

  useEffect(() => {
    onRefresh()
  }, [created])

  return (
    <>
      {
        refresh ?
          (
            <LoadingScreen />
          ) :
          trainings?.length === 0 || typeof trainings === undefined ?
            <ScrollView
              contentContainerStyle={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
              refreshControl={
                <RefreshControl
                  refreshing={refresh}
                  onRefresh={onRefresh}
                />
              }
            >
              <Text
                style={{
                  fontSize: 40,
                  textAlign: 'center',
                  color: 'black'
                }}>No trainings saved
                <Icon2
                  name='emoticon-sad-outline'
                  color='black'
                  size={40} />
              </Text>
            </ScrollView> :
            <View>
              <FlatList
                refreshControl={
                  <RefreshControl
                    refreshing={refresh}
                    onRefresh={onRefresh}
                  />
                }
                data={trainings}
                showsVerticalScrollIndicator={false}
                keyExtractor={(t) => `${t.id}`}
                renderItem={({ item }) => (
                  <ItemComponent training={item} navigation={navigation} key={item.id} />
                )}
              />
              {console.log(user?.userRole.id)}
              {
                
                typeof userId !== 'number' && user?.userRole.id !== 2 ?
                  (<TouchableOpacity
                    style={styles.buttonFloating}
                    onPress={() => { navigation.navigate('AddTrainingWithExercises') }}
                  >
                    <Icon name='plus-a' size={30} color='white' />
                  </TouchableOpacity>) :
                  console.log('')
              }
            </View>
      }
    </>
  )
}

const styles = StyleSheet.create({
  buttonFloating: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    position: 'absolute',
    bottom: 10,
    right: 10,
    height: 70,
    backgroundColor: '#F3872F',
    borderRadius: 100,
  },
  buttonFloating2: {
    // borderWidth: 1,
    // borderColor: 'blue',
    // borderRadius: 100,
    // backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    position: 'absolute',
    top: 0,
    right: 5,
    height: 70,
  }
})

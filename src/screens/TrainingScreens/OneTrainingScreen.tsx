import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext, useEffect, useState } from 'react'
import { Alert, Dimensions, RefreshControl, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Training, Exercise } from '../../interfaces/models/modelAddRoutes';
import Icon from 'react-native-vector-icons/Fontisto';
import DeleteIcon from 'react-native-vector-icons/Entypo';
import { FlatList, TextInput } from 'react-native-gesture-handler';
import { AuthContext } from '../../context/AuthContext';
import { useForm } from '../../hooks/FormHooks/useForm';
import apiConstructor from '../../api/apiConstructor';
import InputSpinner from 'react-native-input-spinner';


interface Props extends StackScreenProps<any, any> { };

const { width: windowWidth, height } = Dimensions.get('window');

export const OneTrainingScreen = ({ route, navigation }: Props) => {

  const { exercises, id, user, name: nameTraining } = route.params as Training;
  const { user: currentUser } = useContext(AuthContext)
  const [clicked, setClicked] = useState(false)
  const [color, setColor] = useState('green')
  const [refresh, setRefresh] = useState(false);
  const [trainingState, setTrainingState] = useState<Training>()

  const { name, sets, reps, trainingId, onChange } = useForm({
    name: '',
    sets: 1,
    reps: 1,
    trainingId: id
  })

  const [useSets, useSetSets] = useState<number>(sets)
  const [useReps, useSetReps] = useState<number>(reps)


  const onPress = () => {
    setClicked(!clicked)

    clicked ?
      setColor('red') : setColor('green')
  }

  const resetForm = () => {
    onChange('', 'name')
    onChange(1, 'sets')
    onChange(1, 'reps')
    setColor('green')
  }

  const onRefresh = () => {
    setRefresh(true);
    const url = `http://152.228.170.234:9001/getTraining/${id}`;

    apiConstructor
      .get<Training>(url)
      .then((x) => {
        setTrainingState(x.data)
        setRefresh(false)
        resetForm()
      })
      .catch(console.log)
  }

  useEffect(onRefresh, [route.params])


  const addExercise = () => {
    apiConstructor
      .post(`http://152.228.170.234:9001/createExercise`, null, {
        params: {
          name,
          reps,
          sets,
          trainingId
        }
      }).then(x => {
        console.log(x.status)
        onRefresh()

      }).catch(console.log)
  }

  const onChangeSets = (val: number) => {
    onChange(val, 'sets')
    useSetSets(val)
  }

  const onChangeReps = (val: number) => {
    onChange(val, 'reps')
    useSetReps(val)
  }

  const onDelete = (id: number) => {

    Alert.alert(
      "Warning",
      "Are you sure you want to delete this exercise?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK", onPress: () => apiConstructor
            .delete(`http://152.228.170.234:9001/deleteExercise/${id}`).then(x => {
              x.status === 200 ?
                onRefresh() :
                console.log('error', x.status, x.statusText)

            }).catch(console.log)
        }
      ]
    );
  }

  return (
    <View style={{backgroundColor: '#F3872F',
      height: height}}
    >
      <View style={{
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 5,
        justifyContent: 'center',
        margin: 10,
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
      }}>
        <Text style={{ fontSize: 30, textAlign: 'center', fontFamily: 'sans-serif-medium' }}>{nameTraining}</Text>
        {
          currentUser?.id === user.id ?
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Text>Creator:</Text>
                <Text style={{ fontWeight: 'bold' }}>@{user.username}</Text>
              </View>
              <DeleteIcon name='add-to-list' size={30} color={color} onPress={onPress} />
            </View> :
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Text>Creator:</Text>
                <Text style={{ fontWeight: 'bold' }}>@{user.username}</Text>
              </View>
            </View>
        }
      </View>
      {
        color === 'red' ?
          <View style={{
            backgroundColor: 'white',
            padding: 10,
            borderRadius: 5,
            justifyContent: 'center',
            margin: 10,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,
            elevation: 6,
          }}>
            <Text>Add exercise</Text>
            <Text>Name:</Text>
            <TextInput
              placeholder='Exercise name'
              onChangeText={(value) => onChange(value, 'name')}
            />
            <Text>Number of sets:</Text>
            <InputSpinner
              max={20}
              min={1}
              step={1}
              value={useSets}
              colorMax={"#f04048"}
              colorMin={"#40c5f4"}
              onDecrease={() => onChangeSets(sets - 1)}
              onIncrease={() => onChangeSets(sets + 1)}
            />
            {/* <TextInput
              placeholder='Enter number of sets'
              keyboardType='numeric'
              onChangeText={(value) => onChangeSets(parseInt(value) )}
            /> */}
            <Text>Number of reps:</Text>
            <InputSpinner
              max={99}
              min={1}
              step={1}
              value={useReps}
              colorMax={"#f04048"}
              colorMin={"#40c5f4"}
              onDecrease={() => onChangeReps(reps - 1)}
              onIncrease={() => onChangeReps(reps + 1)}
            />
            {/* <TextInput
              placeholder='Enter number of reps'
              keyboardType='numeric'
              onChangeText={(value) => onChange(value, 'reps')}
            /> */}
            <TouchableOpacity
              onPress={addExercise}
              style={{
                alignSelf: 'center',
                borderColor: 'black',
                borderWidth: 1,
                padding: 10,
                borderRadius: 10,
                backgroundColor: 'black',
              }}
            >
              <Text style={{ color: 'white', fontWeight: 'bold' }}>Add exercise</Text>
            </TouchableOpacity>
          </View> :
          // <View>
          <FlatList
            scrollEnabled={true}
            data={trainingState?.exercises}
            ListFooterComponent={<View style={{ height: 20 }} />}
            keyExtractor={item => `${item.id}`}
            refreshControl={
              <RefreshControl
                refreshing={refresh}
                onRefresh={onRefresh}
              />
            }
            renderItem={({ item: { id, name, reps, sets } }) => (
              <View style={{
                backgroundColor: 'white',
                padding: 10,
                borderRadius: 5,
                justifyContent: 'center',
                margin: 2
              }}>
                <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>{name}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', padding: 10 }}>
                      <Text style={{ marginHorizontal: 10 }}>Sets: {sets}</Text>
                      <Text style={{ marginHorizontal: 10 }}>Reps: {reps}</Text>
                    </View>
                  </View>
                  {
                    currentUser?.id === user.id ?
                      <View>
                        <DeleteIcon name='cross' size={30} color="#900" onPress={() => onDelete(id)} />
                      </View> :
                      console.log('')
                  }
                </View>
              </View>
            )}
          />
        // </View>
      }

    </View>
  )
}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  // },
  header: {
    // backgroundColor: '#1a1918',
    top: 55,
    alignContent: 'center',
  },
  body: {
    top: 100,
    backgroundColor: '#f2cc8a',
    padding: 20,
    margin: 20,
    borderRadius: 10,
  }

});

import { StackScreenProps } from '@react-navigation/stack';
import React from 'react'
import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { getTrainings } from '../../hooks/GETS/Trainings/getAllTrainingsHook';
import { Training } from '../../interfaces/models/modelAddRoutes';
import Icon from "react-native-vector-icons/Fontisto"
import { TrainingCard } from '../../components/cards/TrainingCard';


interface Props extends StackScreenProps<any, any> {
    idUser?: number;
}

export const AllTrainingScreen = ({ navigation, idUser, route }: Props) => {
    let trainings: Training[] = [];

    const fillTraining = (obj: boolean) => {

        !obj ?
        getTrainings().then(x => {
                x.map(xx => trainings.push(xx))
            })
            :
            getTrainings(idUser).then(x => {
                x.map(xx => trainings.push(xx))
            })
    }

    fillTraining(false);


    return (
        <View>
            <ScrollView>
                {
                    trainings.map((item) => (
                        <TrainingCard training={item} navigation={navigation} key={item.id} />
                    ))
                }
            </ScrollView>

            <TouchableOpacity
                style={styles.buttonFloating}
                // onPress={() => { navigation.navigate('AddNewRouteView') }}
                onPress={() => { navigation.navigate('UserFeedScreen') }}
            >
                <Icon name='plus-a' size={30} color='#01a699' />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    buttonFloating: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 70,
        backgroundColor: '#fff',
        borderRadius: 100,
    }
})

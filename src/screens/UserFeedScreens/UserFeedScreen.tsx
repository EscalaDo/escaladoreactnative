import { StackScreenProps } from '@react-navigation/stack';
import React, { useCallback, useEffect, useState } from 'react'
import { ActivityIndicator, Image, RefreshControl, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
import { ItemComponent } from '../../components/FeedUserComponents/ItemComponent';
import { getRoutes } from '../../hooks/GETS/getAllRoutesHook';
import Icon from "react-native-vector-icons/Fontisto"
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons"
import { Route } from '../../interfaces/models/models';
import { useRutas } from '../../hooks/GETS/Routes/useRutas';
import apiConstructor from '../../api/apiConstructor';
import { LoadingScreen } from '../AuthScreens/LoadingScreen';
// import { apiConstructor } from '../../api/apiConstructor';



interface Props extends StackScreenProps<any, any> {
    userId?: number;
    created?: boolean;
}

export const UserFeedScreen = ({ navigation, userId, created }: Props) => {
    // const { isLoading, rutasList } = useRutas();
    const [routes, setRoutes] = useState<Route[]>()
    const [refresh, setRefresh] = useState(false)

    const fillListRoutes = (obj: Route[]) => {
        setTimeout(() => {
            setRoutes(obj)
            setRefresh(false)
        }, 500);
    }

    const onRefresh = () => {
        setRefresh(true);

        let url = '';

        if (typeof userId !== 'number') {
            url = 'http://152.228.170.234:9001/getAllRoutes';
        }
        else {
            if (!created) {
                url = `http://152.228.170.234:9001/getUserLikedRoutes/${userId}`
            } else {
                url = `http://152.228.170.234:9001/getUserCreatedRoutes/${userId}`
            }
        }

        const resp = apiConstructor
            .get<Route[]>(url)
            .then((x) => {
                fillListRoutes(x.data)
            })

    };

    useEffect(() => {
        onRefresh()
    }, [created])


    return (
        <>
            {
                refresh ?
                    (
                        <LoadingScreen />) :
                    routes?.length === 0 ?
                        <ScrollView
                            contentContainerStyle={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}
                            refreshControl={
                                <RefreshControl
                                    refreshing={refresh}
                                    onRefresh={onRefresh}
                                />
                            }
                        >
                            <Text style={{ fontSize: 40, textAlign: 'center', color: 'black' }}>No routes saved<Icon2 name='emoticon-sad-outline' color='black' size={40} /></Text>
                        </ScrollView> :
                        <View style={{backgroundColor:'white'}}>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refresh}
                                        onRefresh={onRefresh}
                                    />
                                }
                                data={routes}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(ruta) => `${ruta.id}`}
                                renderItem={({ item }) => (
                                    <ItemComponent route={item} created={created} navigation={navigation} key={item.id} />
                                )}
                            />
                            {
                                typeof userId !== 'number' ?
                                    (<TouchableOpacity
                                        style={styles.buttonFloating}
                                        onPress={() => { navigation.navigate('AddNewRouteView') }}
                                    >
                                        <Icon name='plus-a' size={30} color='white' />
                                    </TouchableOpacity>) :
                                    <TouchableOpacity
                                        style={{ display: 'none' }}
                                        onPress={() => { navigation.navigate('AddNewRouteView') }}
                                    >
                                        <Icon name='plus-a' size={30} color='#01a699' />
                                    </TouchableOpacity>
                            }
                        </View>
            }
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-evenly',
        alignContent: 'center',
        flexWrap: 'wrap',
        flexDirection: "row",
        position: 'relative',
        backgroundColor: 'black'
    },
    buttonFloating: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 70,
        position: 'absolute',
        bottom: 10,
        right: 10,
        height: 70,
        backgroundColor: '#F3872F',
        borderRadius: 100,
    }
});

import { StackScreenProps } from '@react-navigation/stack'
import React, { useContext, useEffect, useState } from 'react'
import { Button, Dimensions, FlatList, RefreshControl, ScrollView, StyleSheet, Text, TextInput, View, TouchableOpacity, Keyboard, Alert } from 'react-native'
import apiConstructor from '../../api/apiConstructor'
import { CommentItem } from '../../components/FeedUserComponents/CommentItem';
import { Comment } from '../../interfaces/models/models';
import { LoadingScreen } from '../AuthScreens/LoadingScreen'
import Icon2 from "react-native-vector-icons/MaterialCommunityIcons"
import { AuthContext } from '../../context/AuthContext';



interface Props extends StackScreenProps<any, any> {
    idRoute?: number;
}


const { height, width } = Dimensions.get('screen')
export const CommentsScreen = ({ navigation, route: { params } }: Props) => {

    const { user: currentUser } = useContext(AuthContext);

    const [comments, setComments] = useState<Comment[]>()
    const [refresh, setRefresh] = useState(false)
    const [inputTextValue, setInputTextValue] = useState('')

    const fillListComments = (obj: Comment[]) => {
        setTimeout(() => {
            setComments(obj)
            setRefresh(false)
        }, 500);
    }

    const onPress = () => {
        Keyboard.dismiss();
        let url = `http://152.228.170.234:9001/addComment?description=${inputTextValue}&routeid=${params?.idRoute}&userid=${currentUser?.id}`;
        console.log('TEXT VALUE:',inputTextValue.length)
        inputTextValue.length != 0 ?
            apiConstructor
                .post(url)
                .then(() => {
                    setInputTextValue('')
                    onRefresh();
                })
                .catch(console.log) :
                console.log('empty comment')

    }

    const onRefresh = () => {
        setRefresh(true);
        let url = `http://152.228.170.234:9001/commentsFromRoute/${params?.idRoute}`;

        const resp = apiConstructor
            .get<Comment[]>(url)
            .then((x) => {
                fillListComments(x.data)
            }).catch(console.log)
    };

    useEffect(() => {
        onRefresh()
    }, [params?.idRoute])

    return (

        <>
            {
                refresh ?
                    (
                        <LoadingScreen />
                    ) :
                    comments?.length === 0 ?
                        <ScrollView
                            contentContainerStyle={{ flex: 1, justifyContent: 'space-between' }}
                            refreshControl={
                                <RefreshControl
                                    refreshing={refresh}
                                    onRefresh={onRefresh}
                                />
                            }
                        >
                            <Text style={{ fontSize: 40, textAlign: 'center', color: 'black' }}>No comments added<Icon2 name='emoticon-sad-outline' color='black' size={40} /></Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 2 }}>
                                <TextInput
                                    style={styles.input}
                                    placeholder="Add a comment..."
                                    onChangeText={(text) => setInputTextValue(text)}
                                    value={inputTextValue}
                                />
                                <TouchableOpacity
                                    style={styles.btn}
                                    onPress={onPress}
                                >
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Send</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView> :
                        <View style={{ flex: 1, justifyContent: 'space-between' }}>
                            <FlatList
                                style={{ height: height - 30, backgroundColor: 'white' }}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refresh}
                                        onRefresh={onRefresh}
                                    />
                                }
                                data={comments}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(ruta) => `${ruta.id}`}
                                renderItem={({ item }) => (
                                    <CommentItem navigation={navigation} route={item} key={item.id} />
                                )}
                            />
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 2 }}>
                                <TextInput
                                    style={styles.input}
                                    placeholder="Add a comment..."
                                    onChangeText={(text) => setInputTextValue(text)}
                                    value={inputTextValue}
                                />
                                <TouchableOpacity
                                    style={styles.btn}
                                    onPress={onPress}
                                >
                                    <Text style={{ color: 'white', fontWeight: 'bold' }}>Send</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
            }
        </>
    )
}

const styles = StyleSheet.create({
    input: {
        height: 40,
        width: width * 0.7,
        margin: 12,
        borderWidth: 1,
        borderColor: 'grey',
        borderRadius: 3
    },
    btn: {
        borderColor: 'red',
        padding: 10,
        marginRight: 10,
        borderRadius: 10,
        backgroundColor: 'black',

    }
})

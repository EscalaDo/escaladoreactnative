import { useRef, useState } from 'react'
import { apiConstructor } from '../api/apiConstructor'

export const testServer = () => {
  const [resultTest, setresultTest] = useState<string>('')


  const url = useRef(`http://152.228.170.234:9001/test`);

  const loadMsj = async () => {
    const response = await apiConstructor.get<string>(url.current);
    setresultTest(response.data);
    console.log(resultTest)
  }
  loadMsj();
  return resultTest;
};
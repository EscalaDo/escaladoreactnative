import React from 'react'
import { PermissionsAndroid } from 'react-native';

export const requestCameraPermisionHook = async () => {

    let stateCamera: boolean = false;
    try {
        let msj;

        const isReadRoleGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
        stateCamera = isReadRoleGranted;

        if(isReadRoleGranted){
            console.log('READ_EXTERNAL_STORAGE ROLE IS ACTIVE')
        }else{
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    title: "Cool Photo App Camera Permission",
                    message:
                        "Cool Photo App needs access to your camera " +
                        "so you can take awesome pictures.",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera", granted);
            } else {
                console.log("Camera permission denied", granted);
            }
        }

        
    } catch (err) {
        console.warn(err);
    }

    return stateCamera;

    
}

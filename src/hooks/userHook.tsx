import { useRef, useState } from 'react'
import { apiConstructor } from '../api/apiConstructor'
import { User } from '../interfaces/models/models';

export const GetRandomUserFromAPITest = () => {
    const [userTestResult, setuserTestResult] = useState<User>()
    const url = `http://152.228.170.234:9001/userTest`;

    const getFromApi = async () => {
        const response = await apiConstructor.get<User>(url);
        setuserTestResult(response.data);
    }
    getFromApi();

    return userTestResult;
};
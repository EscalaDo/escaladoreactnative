import React, { useEffect, useState } from 'react'
import apiConstructor from '../../../api/apiConstructor';
import { Route } from '../../../interfaces/models/models';

export const useRutas = () => {
    const [isLoading, setisLoading] = useState(true)
    const [rutasList, setRutasList] = useState<Route[]>([])
    const url = 'http://152.228.170.234:9001/getAllRoutes';

    const loadRutas = async() => {
        setisLoading(true);
        const resp = await apiConstructor.get<Route[]>(url);
        
        mapRouteList(resp.data)
    }

    const mapRouteList = ( routesList: Route[] ) => {
        setRutasList([...routesList])
    }

    useEffect(() => {
        loadRutas();
        setisLoading(false);
    }, [])
    
    return {
        isLoading,
        rutasList
    }
}

import React, { MutableRefObject, useEffect, useState } from 'react'
import { useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import apiConstructor from '../../api/apiConstructor';
import { Route } from '../../interfaces/models/models';

const getAllRoutesHook = async (idUser?: number) => {
  const [result, setResult] = useState<Route[]>([])
  let url:MutableRefObject<string>;
  typeof idUser === 'number' ?
    url = useRef(`http://152.228.170.234:9001/getUserLikedRoutes/${idUser}`) :
    url = useRef('http://152.228.170.234:9001/getAllRoutes')

  useEffect(() => {
    const getRoutesPetition = async () => {
      
      await apiConstructor
        .get<Route[]>(url.current).
        then(x => setResult(x.data));
    }

    getRoutesPetition();
  }, [])


  return result;
}

export function getRoutes(idUser?: number) {

  const routes = getAllRoutesHook(idUser);

  return routes;
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 6,
    elevation: 3,
    backgroundColor: '#fff',
    shadowOffset: { width: 1, height: 1 },
    shadowColor: '#333',
    shadowOpacity: 0.3,
    margin: 2,
  },
});
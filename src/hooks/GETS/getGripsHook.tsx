import React, { useEffect, useState } from 'react'
import { useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import apiConstructor from '../../api/apiConstructor';
import { Grip } from '../../interfaces/models/modelAddRoutes';

export const getAllGripsHook = () => {
  const [result, setResult] = useState<Grip[]>([])

  const url = useRef('http://152.228.170.234:9001/getGripTypes');

  useEffect(() => {
    const getGripPetition = async () => {
      const { data, status } = await apiConstructor.get<Grip[]>(url.current);
      setResult(data);
    }

    getGripPetition();
  }, [])


  return result;
}

export function getGrips() {

  const grip = getAllGripsHook();

  return grip;
}
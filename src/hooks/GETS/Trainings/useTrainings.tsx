import React, { useEffect, useState } from 'react'
import apiConstructor from '../../../api/apiConstructor';
import { Training } from '../../../interfaces/models/modelAddRoutes';

export const useTrainings = () => {
    const [isLoading, setisLoading] = useState(true)
    const [trainingsList, setTrainingsList] = useState<Training[]>([])
    const url = 'http://152.228.170.234:9001/getAllTrainings';

    const loadRutas = async () => {
        setisLoading(true);
        const resp = await apiConstructor.get<Training[]>(url);

        mapRouteList(resp.data)
    }

    const mapRouteList = (trainingList: Training[]) => {
        setTrainingsList([...trainingList])
    }

    useEffect(() => {
        loadRutas();
        setisLoading(false);
    }, [])

    return {
        isLoading,
        trainingsList
    }
}

import React, { MutableRefObject, useEffect, useRef, useState } from 'react'
import { StyleSheet } from 'react-native';
import apiConstructor from '../../../api/apiConstructor';
import { Training } from '../../../interfaces/models/modelAddRoutes';

export const getAllTrainingsHook = async (idUser?: number) => {
    const [result, setResult] = useState<Training[]>([])
    let url: MutableRefObject<string>;
    typeof idUser === 'number' ?
        url = useRef(`http://152.228.170.234:9001/getUserLikedTrainings/${idUser}`) :
        url = useRef('http://152.228.170.234:9001/getAllTrainings')

    useEffect(() => {
        const getTrainingPetition = async () => {

            await apiConstructor
                .get<Training[]>(url.current).
                then(x => setResult(x.data));
        }

        getTrainingPetition();
    }, [])


    return result;
}

export function getTrainings(idUser?: number) {

    const routes = getAllTrainingsHook(idUser);

    return routes;
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 6,
        elevation: 3,
        backgroundColor: '#fff',
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        margin: 2,
    },
});

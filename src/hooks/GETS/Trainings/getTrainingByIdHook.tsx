import React, { useEffect, useRef, useState } from 'react'
import apiConstructor from '../../../api/apiConstructor';
import { Training } from '../../../interfaces/models/modelAddRoutes';

const getTrainingByIdHook = (idTraining:number) => {
    const [result, setResult] = useState<Training>()

    const url = useRef(`http://152.228.170.234:9001/getTraining/${idTraining}`);
  
    useEffect(() => {
      const getTrainingPetition = async () => {
        const { data, status } = await apiConstructor.get<Training>(url.current);
        setResult(data);
      }
  
      getTrainingPetition();
    }, [])
    return result;
}

export function getTraining(idTraining:number) {

    const training = getTrainingByIdHook(idTraining);
  
    return training;
  }

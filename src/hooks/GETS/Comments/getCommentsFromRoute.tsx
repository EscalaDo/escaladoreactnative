import React, { useEffect, useRef, useState } from 'react'
import { RESULTS } from 'react-native-permissions';
import apiConstructor from '../../../api/apiConstructor';
import { Comment } from '../../../interfaces/models/modelForComments';

export const getCommentsFromRoute = async (id:number) => {
    const [result, setResult] = useState<Comment[]>([])
    const url = useRef(`http://152.228.170.234:9001/commentsFromRoute/${id}`);

    useEffect(() => {
      const getCommentsPetition = async () => {
        await apiConstructor
          .get<Comment[]>(url.current).
          then(x => setResult(x.data));
      }
  
      getCommentsPetition();
    }, [])
  
  
    return result;
  }

export async function getComments(id:number) {
    const comments = await getCommentsFromRoute(id);
    return comments;
}

import React, { useEffect, useState } from 'react'
import { useRef } from 'react';
import apiConstructor from '../../api/apiConstructor';
import { Grade } from '../../interfaces/models/modelAddRoutes';

export const getAllGradesHook = () => {
  const [result, setResult] = useState<Grade[]>([])

  const url = useRef('http://152.228.170.234:9001/getGrades');

  useEffect(() => {
    const getGradePetition = async () => {
      const { data, status } = await apiConstructor.get<Grade[]>(url.current);
      setResult(data);
    }

    getGradePetition();
  }, [])
  return result;
}

export function getGrades() {

  const grade = getAllGradesHook();

  return grade;
}

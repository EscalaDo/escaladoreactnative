import React, { useEffect, useState } from 'react'
import { useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import  apiConstructor  from '../../api/apiConstructor';
import { Skill } from '../../interfaces/models/modelAddRoutes';
import { Role } from '../../interfaces/models/models';

export const getRolesHook = () => {
  const [result, setResult] = useState<Role[]>([])

  const url = useRef('http://152.228.170.234:9001/getRoles');

  useEffect(() => {
    const getRolesPetition = async () => {
      const { data, status } = await apiConstructor.get<Role[]>(url.current);
      setResult(data);
    }

    getRolesPetition();
  }, [])


  return result;
}

export function getRoles() {

  const roles = getRolesHook();

  return roles;
}
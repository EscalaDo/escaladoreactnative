import React, { useEffect, useRef, useState } from 'react'
import apiConstructor from '../../api/apiConstructor';
import { ClimbingGym } from '../../interfaces/models/models';

export const getAllClimbingGymHook = () => {
    const [result, setResult] = useState<ClimbingGym[]>([])

    const url = useRef('http://152.228.170.234:9001/climbingGyms');

    useEffect(() => {
        const getClimbingGymetition = async () => {
            const { data, status } = await apiConstructor.get<ClimbingGym[]>(url.current);
            setResult(data);
            console.log(data)
        }


        getClimbingGymetition();
    }, [])
    return result;
}

export function getClimbingGyms() {

    const gym = getAllClimbingGymHook();

    return gym;
}
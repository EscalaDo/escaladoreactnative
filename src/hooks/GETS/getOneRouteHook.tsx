import React, { useEffect, useState } from 'react'
import { useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { apiConstructor } from '../../api/apiConstructor';
import { Route } from '../../interfaces/models/models';

export const getOneRouteHook = (idRoute: number) => {
    const [result, setResult] = useState<Route>();
    const [url, setUrl] = useState<string>(`http://152.228.170.234:9001/getRoute/${idRoute}`);
    console.log('RUTA A BUSCAR:###~~~~', result)
    useEffect(() => {
        const getRoutesPetition = async () => {
            const { data, status } = await apiConstructor.get<Route>(url);
            setResult(data);
        }

        getRoutesPetition();
    },[])


    return result;
}

export function getOneRoute(idRoute: number) {
    // const routes = getOneRouteHook(idRoute);
    console.log('IDRUTA @@@@@@@@@@@@@@@@@@@@', idRoute)
    // console.log('RUTA @@@@@@@@@@@@@@@@@@@@', routes)
    return getOneRouteHook(idRoute);
}
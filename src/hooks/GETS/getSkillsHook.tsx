import React, { useEffect, useState } from 'react'
import { useRef } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import  apiConstructor  from '../../api/apiConstructor';
import { Skill } from '../../interfaces/models/modelAddRoutes';

export const getAllSkillsHook = () => {
  const [result, setResult] = useState<Skill[]>([])

  const url = useRef('http://152.228.170.234:9001/getSkills');

  useEffect(() => {
    const getskillPetition = async () => {
      const { data, status } = await apiConstructor.get<Skill[]>(url.current);
      setResult(data);
    }

    getskillPetition();
  }, [])


  return result;
}

export function getSkills() {

  const skill = getAllSkillsHook();

  return skill;
}

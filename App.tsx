import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect, useState } from 'react'
import { requestCameraPermisionHook } from './src/hooks/requestCameraPermisionHook';
import { LoginOrSignUp } from './src/screens/LoginOrSignUp';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Route } from './src/interfaces/models/models';
import { getRoutes } from './src/hooks/GETS/getAllRoutesHook';
import { Text, TextInput, View } from 'react-native';
import { AllRoutes } from './src/screens/AllRoutesView';
import { Navigator } from './src/navigator/Navigator';
import { RouteImageComponent } from './src/components/routeImageComponent';
import { useGetTokenUser } from './src/hooks/GETS/User/useGetTokenUser';
import { LoginScreen } from './src/screens/AuthScreens/LoginScreen';
import { AuthProvider } from './src/context/AuthContext';


const Tab = createBottomTabNavigator();

const AppState = ({ children }: any) => {
  return (
    <AuthProvider>
      {children}
    </AuthProvider>
  )
}


const App = () => {


  // const {resp} = useGetTokenUser();

  // console.log('Inshallah',resp)

  return (
    <NavigationContainer>
      <AppState>
        <Navigator />
      </AppState>
    </NavigationContainer>
    // <LoginScreen navigation={undefined} route={undefined} />
  );
}

export default App;
